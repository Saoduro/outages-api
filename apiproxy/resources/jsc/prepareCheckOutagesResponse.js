var response = context.getVariable("response.content");
var premiseNumber = context.getVariable("premiseNumbers");
var premiseNum = context.getVariable("premiseNumber");
var correlationId = context.getVariable("correlationId");

var responseBody = JSON.parse(response);

var response = {};
var outages = [];
var outageObject = {};

response.correlationId = correlationId;
outageObject.premiseNumber = premiseNum;
outageObject.reported = true;

if (responseBody.trackerResponse.Status.ReasonCode === 0 && responseBody.trackerResponse.Status.ReasonDescription.includes("Success")) {
    outageObject.estimatedRestorationTimeFound = true;
    outageObject.estimatedRestorationTime = responseBody.trackerResponse.Data.ert;
}

if (responseBody.trackerResponse.Status.ReasonCode == 7 && responseBody.trackerResponse.Status.ReasonDescription.includes("Power has been restored at")) {
    outageObject.restored = true;
    outageObject.restoredTime = responseBody.trackerResponse.Data.outage_restored_time;
}

if (responseBody.trackerResponse.Status.ReasonCode == 2 && responseBody.trackerResponse.Status.ReasonDescription.includes("No outage logged for specific premiseID")) {
    outageObject.reported = false;
    outageObject.estimatedRestorationTimeFound = false;
}

if (responseBody.trackerResponse.Status.ReasonCode == 1 && responseBody.trackerResponse.Status.ReasonDescription.includes("Success ERT Not Found")) {
    outageObject.estimatedRestorationTimeFound = false;
}

if (responseBody.trackerResponse.Status.ReasonCode == -1) {
    outageObject.reported = false;
    context.setVariable("response.status.code", 404);
}

outages.push(outageObject);

var premiseNums = premiseNumber.split(",");

for(var index = 1; index < premiseNums.length; index++) {
    var endpoint = properties.endpoint;
    var url = context.getVariable(endpoint) + '?premiseNumber=' + premiseNums[index];
    var myRequest = new Request(url, "GET");
   
    var exchange = httpClient.send(myRequest);
    exchange.waitForComplete();
    
    // Get and Process the response
    if (exchange.isSuccess()) {
        var responseObj = exchange.getResponse().content.asXML;
        var json = E4XtoJSON(responseObj);
        var data = JSON.stringify(json, null, 2);
        var outageObject = {};
        outageObject.premiseNumber = premiseNums[index];
        outageObject.reported = true;
        if (json.Status.ReasonCode === 0 && json.Status.ReasonDescription.includes("Success")) {
            outageObject.estimatedRestorationTimeFound = true;
            outageObject.estimatedRestorationTime = json.Data.ert;
        }
        
        if (json.Status.ReasonCode == 7 && json.Status.ReasonDescription.includes("Power has been restored at")) {
            outageObject.restored = true;
            outageObject.restoredTime = json.Data.outage_restored_time;
        }
        
        if (json.Status.ReasonCode == 2 && json.Status.ReasonDescription.includes("No outage logged for specific premiseID")) {
            outageObject.reported = false;
            outageObject.estimatedRestorationTimeFound = false;
        }
        
        if (json.Status.ReasonCode == 1 && json.Status.ReasonDescription.includes("Success ERT Not Found")) {
            outageObject.estimatedRestorationTimeFound = false;
        }
        
        if (json.Status.ReasonCode == -1) {
            outageObject.reported = false;
            context.setVariable("response.status.code", 404);
        }
        outages.push(outageObject);
    } else if (exchange.isError()) {
         context.setVariable("myResponseError", exchange.getError());
    }
    
}

response.outages = outages;


context.setVariable("response.content", JSON.stringify(response));
