var premiseNumber = context.getVariable("premiseNumbers");
var correlationId = context.getVariable("request.queryparam.correlationId");
var mock = context.getVariable("request.queryparam.mock");
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);

var mockResponse = {};
var outages = [];


mockResponse.correlationId = correlationId;
var premiseNums = premiseNumber.split(",");

if (mock == "success") {
    for(var index = 0; index < premiseNums.length; index++) {
        var outageObject = {};
        outageObject.premiseNumber = premiseNums[index];
        outageObject.reported = true;
        outageObject.estimatedRestorationTimeFound = true;
        outageObject.estimatedRestorationTime = timeStamp;
        outages.push(outageObject);
    }
} else {
    for(var index = 0; index < premiseNums.length; index++) {
        var outageObject = {};
        outageObject.premiseNumber = premiseNums[index];
        outageObject.reported = false;
        outageObject.estimatedRestorationTimeFound = false;
        outages.push(outageObject);
    }
}

mockResponse.outages = outages;

context.setVariable("response.content", JSON.stringify(mockResponse));

