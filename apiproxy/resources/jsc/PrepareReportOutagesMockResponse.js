var premiseNumber = context.getVariable("premiseNumber");
var correlationId = context.getVariable("correlationId");
var mock = context.getVariable("mock");
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);


var mockResponse = {};
var outages = [];
var outageObject = {};

mockResponse.correlationId = correlationId;

if (mock == "success") {
    outageObject.premiseNumber = premiseNumber;
    outageObject.reported = true;
    outageObject.reportedTime = timeStamp;
    context.setVariable("response.status.code", 201);
} else {
    outageObject.premiseNumber = premiseNumber;
    outageObject.reported = false;
    outageObject.reportedReason = "Customer account disconnected.";
}

outages.push(outageObject);
mockResponse.outages = outages;

context.setVariable("response.content", JSON.stringify(mockResponse));
context.setVariable('response.header.Content-Type', "application/json");

