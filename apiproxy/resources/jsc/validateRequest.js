var premiseNumber = context.getVariable("request.queryparam.premiseNumber");
var mock = context.getVariable("request.queryparam.mock");
context.setVariable("isValidRequest", true);

var regex = new RegExp("^[0-9]{4,}$");

if(premiseNumber !== null) {
    var premiseNums = premiseNumber.split(",");
    context.setVariable("correlationId", context.getVariable("request.queryparam.correlationId"));
    context.setVariable("premiseNumber", premiseNums[0]);    
    for( var index = 0; index < premiseNums.length; index++) {
       if( !regex.test(premiseNums[index]) ) {
            context.setVariable("isValidRequest", false);
            context.setVariable("queryParam", "premiseNumber");
            context.setVariable("errorMessage", "Please provide a valid premiseNumber : (" + premiseNums[index] + ") it should be numeric with minimum 4 numbers");
            break;
        }
    }
} else {
    context.setVariable("isValidRequest", false);
    context.setVariable("queryParam", "premiseNumber");
    context.setVariable("errorMessage", "Please provide a valid premiseNumber");
} 
if (mock !== null && context.getVariable("isValidRequest") === true) {
    if (mock != "success" && mock != "error" ) {
        context.setVariable("isValidRequest", false);
        context.setVariable("queryParam", "mock");
        context.setVariable("errorMessage", "Please provide the valid mock value");
    }
}


var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);
//this value will be used in target flow
context.setVariable("premiseNumbers", premiseNumber);