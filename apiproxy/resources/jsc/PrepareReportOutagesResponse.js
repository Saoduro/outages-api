 //Created Variables to get data from requestQueryParam and response from Backend
var response = context.getVariable("response.content");
var premiseNumber = context.getVariable("request.queryparam.strPremiseNumber");
var correlationId = context.getVariable("correlationId");
var timeStamp = new Date().toISOString().replace('Z', '');
var responseBody = JSON.parse(response);

response = {};
var outages = [];
var outageObject = {};

// Preparing the Proxy response Body
response.correlationId = correlationId;
outageObject.premiseNumber = premiseNumber;

print("Status code: "+responseBody.OperationResult.Status.StatusCode);
if (responseBody.OperationResult.Status.StatusCode === 0) {
    context.setVariable("response.status.code", 201);
    outageObject.reported = true;
    outageObject.reportedTime = timeStamp;
    outages.push(outageObject);
    response.outages = outages;
}
else if(responseBody.OperationResult.Status.StatusCode == 1) {
    outageObject.reported = false;
    outageObject.reportedReason = "Outage already exists.";
    outages.push(outageObject);
    response.outages = outages;
}
else if(responseBody.OperationResult.Status.StatusCode == 5) {
    outageObject.reported = false;
    outageObject.reportedReason = "Customer account is disconnected.";
    outages.push(outageObject);
    response.outages = outages;
}
else if(responseBody.OperationResult.Status.StatusCode == 3) {
    context.setVariable("isValidResponse", false);
}

context.setVariable("response.content", JSON.stringify(response));